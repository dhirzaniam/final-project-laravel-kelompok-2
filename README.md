## Final Project

## Kelompok 2

## Anggota Kelompok

<ul>
    <li>Dhiya' Akhfiya' Rosikhuna Fil'ilmi</li>
    <li>Daffa Hirza Ni'am</li>
    <li>Ilham Azrinargana Pulungan</li>
</ul>

## Tema project

E-Commerce Barang Rumah Tangga

### ERD
![ERD](/uploads/5ebf80389e54497e7f3b3fcdcaa5d8e8/ERD.jpeg)

## Link Video
Link Demo Aplikasi = https://youtu.be/WbARb-Qrp5M

