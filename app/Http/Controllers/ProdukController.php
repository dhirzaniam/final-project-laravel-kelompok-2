<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
    public function create(){
        return view('produk.tambah');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:3',
            'deskripsi' => 'required',
            'harga' => 'required|min:3',
            'category_id' => 'required|min:3',
        ]);

        DB::table('produk')->insert([
            'nama' => $request->input('nama'),
            'deskripsi' => $request->input('deskripsi'),
            'harga' => $request->input('harga'),
            'category_id' => $request->input('category_id'),
        ]);

        return redirect('/produk');
    }
}
