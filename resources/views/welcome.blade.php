@extends('layouts.main')
@section('title')
    Selamat Datang di Mini E-Commerce Kelompok 2!
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
@endpush

@push('styles')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
@endpush

@section('content')
<div class="col-10 col-md-12" carousel-indicator-width:"15px">
          <div id="carouselExample" class="carousel slide" width = 200% >
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="https://images.tokopedia.net/img/cache/1208/NsjrJu/2023/11/6/8b1dae56-8cc2-45b6-bf13-2a09d2925bee.jpg.webp?ect=4g" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="https://images.tokopedia.net/img/cache/1208/NsjrJu/2023/11/8/ffbd5915-3d07-4d57-9b07-e26b8de2944a.jpg.webp?ect=4g" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="https://images.tokopedia.net/img/cache/1208/NsjrJu/2023/11/6/a0953aa5-6054-4bea-8b65-bd1d753a013a.jpg.webp?ect=4g" class="d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next" >
              <span class="carousel-control-next-icon" aria-hidden="true" ></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
            </div>
          </div>
        </div>
        
        <div class="card-group my-3">
            <div class="card">
              <img src="https://images.tokopedia.net/img/cache/300-square/VqbcmM/2023/6/21/0e8ba679-74f1-4c2c-b685-ebffee533db3.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">RMZ City Jeep Wrangler Rubicon 2021 Skala 1:36 - Hitam</h5>
                <p class="card-text">Kondisi: Baru
                  Min. Pemesanan: 1 Buah
                  Etalase: RMZ City
                  Material : Besi dan Plastik
                  Ukuran: 1:36
                  Usia : 3 Tahun keatas
                  Ukuran : P : 12,2 cm, L : 5,5 cm, T : 5,1 cm
                  Pintu dapat dibuka
                  Tidak Pull Back (Free Wheel)</p>
                  <button type="button" class="btn btn-dark">Buy</button>
              </div>
              <div class="card-footer">
                <small class="text-body-secondary">Last updated 3 mins ago</small>
              </div>
            </div>
            <div class="card">
              <img src="https://images.tokopedia.net/img/cache/300-square/VqbcmM/2023/10/31/aff647ed-fc90-4bc9-b5eb-124d56ca016a.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">ecentio 2 cup Juicer Portable Blender Portable kaca 8 mata pisau 720ml</h5>
                <p class="card-text">Kondisi: Baru
                  Min. Pemesanan: 1 Buah
                  Etalase: 1111 SUPER PROMO
                  Hai sayang
                  Selamat datang di toko ecentio
                  Toko ecentio akan melayani Anda 24 jam sehari, jangan ragu untuk belanja di toko kami❤
                  (tersedia untuk harga grosir, silahkan lanjut hubungi kami)</p>
                  <button type="button" class="btn btn-dark">Buy</button>
              </div>
              <div class="card-footer">
                <small class="text-body-secondary">Last updated 3 mins ago</small>
              </div>
            </div>
            <div class="card">
              <img src="https://images.tokopedia.net/img/cache/300-square/VqbcmM/2022/2/2/9feb44a9-c884-43b0-b558-ad17b5e1eacf.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Playgro Baby Twisting Barbel Rattle - Mainan Gigit Anak Bayi Balita</h5>
                <p class="card-text">Kondisi: Baru
                  Min. Pemesanan: 1 Buah
                  Etalase: Special Deals Fashion Home Travel
                  Age 3 + months Dimensions and Size: 12.7 x 5.1 x 4.4 cm Inside the transparent multi coloured rattle rattling balls In the transparent ball spinning disk with pictures of animals Multi coloured ball is spinning with thirst On the rod strung two diversified one ring, which can be moved</p>
                <button type="button" class="btn btn-dark">Buy</button>
                </div>
              <div class="card-footer">
                <small class="text-body-secondary">Last updated 3 mins ago</small>
              </div>
            </div>
          </div>
        </div>
@endsection
